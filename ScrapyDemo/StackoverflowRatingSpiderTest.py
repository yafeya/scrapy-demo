import requests
import scrapy
from ScrapyDemo.spiders.stackoverflow import StackoverflowSpider

class Response:
    def __init__(self, body, url):
        self.body = body
        self.url = url

if __name__ == '__main__':
    url = 'https://stackoverflow.com/questions?tab=votes'
    raw = requests.get(url)
    response = Response(raw.text, raw.url)
    spider = StackoverflowSpider()
    spider.parse(response)

