# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


class ScrapydemoPipeline:
    def process_item(self, item, spider):
        # here is used to save to db.
        print('save auther: {item_auther}, votes: {item_votes}, answers: {item_answers} to db'.format(item_auther=item['auther'], item_votes=item['votes'],item_answers=item['answers']))
        return item
