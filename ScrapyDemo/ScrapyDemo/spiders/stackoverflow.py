from tokenize import Number
import scrapy
from ScrapyDemo.items import StackOverflowDocument
from bs4 import BeautifulSoup, Tag
import re

class StackoverflowSpider(scrapy.Spider):
    name = 'stackoverflow'
    allowed_domains = ['stackoverflow.com']
    start_urls = ['https://stackoverflow.com/questions?sort=votes']

    def __map_item__(self, node: Tag):
        item = StackOverflowDocument()
        item['votes'] = node.select_one('.s-post-summary--stats-item__emphasized .s-post-summary--stats-item-number').contents[0]
        item['answers'] = node.select_one('.has-answers .s-post-summary--stats-item-number').contents[0]
        item['views'] = node.select_one('.is-supernova .s-post-summary--stats-item-number').contents[0]
        auther_element = node.select_one('.s-user-card--link a')
        item['auther'] = 'Stackoverflow' if auther_element is None else auther_element.contents[0]
        item['autherLink'] = '' if auther_element is None else 'https://{domain}{relative_url}'.format(domain=self.allowed_domains[0], relative_url=auther_element['href']) 
        title_element = node.select_one('.s-post-summary--content-title a')
        item['title'] = title_element.contents[0]
        item['url'] = 'https://{domain}{relative_url}'.format(domain=self.allowed_domains[0] ,relative_url= title_element['href'])
        item['description'] = node.select_one('.s-post-summary--content-excerpt').contents[0]
        return item

    def parse(self, response):
        response_body: str = response.body
        soup = BeautifulSoup(response_body, 'lxml')
        nodeList = soup.select('.s-post-summary.js-post-summary')
        for node in nodeList:
            item = self.__map_item__(node)
            yield item
        
        result=re.search('&page=(?P<page>[0-9]+)', response.url)
        page = 1 if result is None else int(result.group('page'))  
        while page < 11:
            page+=1
            next_page_url= '{base_url}&page={page}'.format(base_url=self.start_urls[0], page=page)
            yield scrapy.Request(url=next_page_url, callback=self.parse)
 
