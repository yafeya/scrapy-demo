# 1. 安装`Scrapy`

```bash
pip3 install Scrapy
```

# 2. 创建项目

### 2.1 创建项目命令

```bash
# <projectName>为要创建的Scrapy项目名称，必须全部由英文字母组成
scrapy startproject <projectName> 
```

### 2.2 下一步提示

![next-step](./images/hint.png)

### 2.2 创建`Spider`

```bash
scrapy genspider example example.com
```

> 这个语句用来创建一个`Spider`，用来爬取`example.com`上的数据

# 3. 默认生成文件

- items.py \
  用来定义要获取的字段
- pipelines.py \
  用来定义存储，比如获取到的数据保存到哪里
- settings.py \
  对于app的设置
- spider文件夹 \
  爬取数据的逻辑

# 4. 框架介绍

![scrapy-architect](./images/scrapy_architecture.png)

### 4.1 Scrapy Engine

核心组件，负责其他组件的数据传递，也就是说\`Spider\`, \`Scheduler\`, \`Downloader\` 和 \`ItemPipeline\`的通信和协调都要靠\`Scrapy Engine\`来完成。

### 4.2 Scheduler

负责处理Engine方面发送的request，将这些request放入队列，一遍一个一个的处理。

### 4.3 Downloader

通过Scheduler的Request下载相对应的Response。

### 4.4 Scrapy Spider

- 责处理第一个告诉Engine要从那个网站爬取数据，以及第一个要爬取的URL是什么
- 负责处理Downloader下载的Response
- 将response生成Item，发送给pipeline

### 4.5 Pipeline

接收到来自于Spider的Item，并处理Item（存储到数据库，或者文件）。

### 4.6 Middleware

Downloader Middleware和Spider Middleware，可以理解为Downloader组件和Spider组件的辅助工具。

### 4.7 工作顺序

![scrapy-architect](./images/scrapy-flow.png)